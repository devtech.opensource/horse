package org.devtech.horse.persistence.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.devtech.horse.persistence.enums.Gender;
import org.devtech.horse.persistence.utils.HorseConstants;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import java.time.LocalDate;

@Getter
@Setter
@Entity
@Table(name = "user", schema = HorseConstants.SchemaNames.HORSE)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User extends Identifiable<Integer> {

    private static final long serialVersionUID = 7769474167792478109L;
    private static final String FULL_NAME = "full_name";
    private static final String USER_NAME = "user_name";
    private static final String ENCRYPTED_PASSWORD = "encrypted_password";
    private static final String EMAIL = "email";
    private static final String IS_EMAIL_ACTIVE = "is_email_active";
    private static final String BIRTH_DATE = "birth_date";
    private static final String PHONE_NUMBER = "phone_number";
    private static final String GENDER = "gender";

    @Column(name = FULL_NAME, nullable = false, length = 64)
    private String fullName;

    @Column(name = USER_NAME, nullable = false, length = 64)
    private String userName;

    @Column(name = ENCRYPTED_PASSWORD, nullable = false, length = 36)
    private String encryptedPassword;

    @Column(name = EMAIL, nullable = false, length = 64)
    private String email;

    @Column(name = IS_EMAIL_ACTIVE, nullable = false)
    private Boolean isEmailActive;

    @Column(name = BIRTH_DATE, nullable = false)
    private LocalDate birthDate;

    @Column(name = PHONE_NUMBER, nullable = false, length = 32)
    private String phoneNumber;

    @Enumerated(EnumType.STRING)
    @Column(name = GENDER, nullable = false, length = 10)
    private Gender gender;
}
