package org.devtech.horse.persistence.utils;

public final class HorseConstants {

    public static final class SchemaNames {

        public static final String HORSE = "horse";

        private SchemaNames() {

        }
    }


    private HorseConstants() {

    }
}
