package org.devtech.horse.persistence.enums;

public enum Gender {

    MAN,
    WOMAN;
}
