package org.devtech.horse.persistence.configs;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories("org.devtech.horse.persistence.repositories")
@EntityScan("org.devtech.horse.persistence.models")
public class PersistenceConfig {
}
