package org.devtech.horse.business.domains.user.model;

import java.io.Serializable;
import java.time.LocalDate;


public class UserModel implements Serializable {

    private String fullName;
    private String userName;
    private String email;
    private Boolean isEmailActive;
    private LocalDate birthDate;
    private String phoneNumber;
    private String gender;
}
