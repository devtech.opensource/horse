package org.devtech.horse.business.domains.user.referential;

import org.devtech.horse.business.domains.user.model.UserModel;

public interface UserReferential {

    UserModel save(UserModel userModel);
}
