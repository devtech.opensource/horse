package org.devtech.horse.business.utils.pagination;

import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Page<T extends Serializable> implements Serializable {

    private final long totalElements;
    private final int totalPages;
    private final List<T> elements;

    private Page(Builder<T> builder) {
        this.totalElements = builder.totalElements;
        this.totalPages = builder.totalPages;
        this.elements = builder.elements;
    }

    public static <T extends Serializable> Builder<T> getBuilder() {
        return new Builder<>();
    }

    public long getTotalElements() {
        return totalElements;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public List<T> getElements() {
        return elements;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class Builder<T extends Serializable>{

        private long totalElements;
        private int totalPages;
        private List<T> elements = new ArrayList<>();

        private Builder() {
        }

        public Builder<T> totalElements(long totalOfElements) {
            this.totalElements = totalOfElements;
            return this;
        }

        public Builder<T> totalPages(int totalPages) {
            this.totalPages = totalPages;
            return this;
        }

        public Builder<T> elements(List<T> elements) {
            this.elements = elements;
            return this;
        }

        public Page<T> build() {
            return new Page<>(this);
        }
    }
}
