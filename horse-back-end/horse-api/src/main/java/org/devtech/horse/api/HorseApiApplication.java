package org.devtech.horse.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "org.devtech.horse")
public class HorseApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(HorseApiApplication.class, args);
    }

}
