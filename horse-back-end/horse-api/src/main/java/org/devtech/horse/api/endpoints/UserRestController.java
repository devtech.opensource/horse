package org.devtech.horse.api.endpoints;

import org.devtech.horse.business.domains.user.service.UserService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserRestController {

    private final UserService userService;

    UserRestController(UserService userService){
      this.userService=userService;
    }
}
