package org.devtech.horse.api.configs;

import org.devtech.horse.business.domains.user.referential.UserReferential;
import org.devtech.horse.business.domains.user.service.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HorseBeansConfig {

    private final UserReferential userReferential;


    HorseBeansConfig(UserReferential userReferential){
      this.userReferential=userReferential;
    }

    @Bean
    UserService userService(){
        return new UserService(userReferential);
    }
}
