package org.devtech.horse.adapter.mappers;

import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.toList;


/**
 * Generic mapper for mapping from/to entity and domain model.
 *
 * @param <E> the entity type.
 * @param <M>  the model type.
 */
public interface Mapper<E extends Serializable, M extends Serializable> {

    String SPRING = "spring";

    E mapToEntity(M model);

    M mapToModel(E entity);

    default List<E> mapToEntities(Collection<M> models) {
        if (CollectionUtils.isEmpty(models)) {
            return Collections.emptyList();
        }
        return models.stream()
                .map(this::mapToEntity)
                .collect(toList());
    }

    default List<M> mapToModels(Collection<E> entities) {
        if (CollectionUtils.isEmpty(entities)) {
            return Collections.emptyList();
        }
        return entities.stream()
                .map(this::mapToModel)
                .collect(toList());
    }
}

