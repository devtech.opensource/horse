package org.devtech.horse.adapter.adapters;

import org.devtech.horse.adapter.mappers.user.UserMapper;
import org.devtech.horse.business.domains.user.model.UserModel;
import org.devtech.horse.business.domains.user.referential.UserReferential;
import org.devtech.horse.persistence.repositories.UserRepository;
import org.springframework.stereotype.Component;

@Component
public class UserAdapter implements UserReferential {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    UserAdapter(UserRepository userRepository,
                UserMapper userMapper){
       this.userRepository=userRepository;
       this.userMapper=userMapper;
    }

    @Override
    public UserModel save(UserModel userModel) {
        return userMapper.mapToModel(userRepository.save(userMapper.mapToEntity(userModel)));
    }
}
