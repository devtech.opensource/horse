package org.devtech.horse.adapter.mappers.user;

import org.devtech.horse.adapter.mappers.Mapper;
import org.devtech.horse.business.domains.user.model.UserModel;
import org.devtech.horse.persistence.models.User;

@org.mapstruct.Mapper(componentModel = Mapper.SPRING)
public interface UserMapper extends Mapper<User, UserModel> {
}
